class Inn:
    """
    Creating a class to validate the entries a
    user is making to input data gathered for
    customers that are staying at the inn.
    """
    def __init__(self,night_count, guest_count, room_count, dollar_count, rooms_available = 0):
        """
        Initializing the following attributes
        """
        self.night_count = night_count
        self.guest_count = guest_count
        self.room_count = room_count
        self.dollar_count = dollar_count
        self.rooms_available = rooms_available
    def customer_checin(self,time):
        """
        A method that portrays the act 
        of a customer checking out of the hotel.
        """
        if time.isdecimal():
            print(f"Your arrival time is {time}, here are your keys!")
            return True
        else:
            print(f"{time} is incorrect, please enter a new value!")
            return False    
    def customer_checout(self,time):
        """
        A method that portrays the act 
        of a customer checking out of the hotel.
        """
        if time.isdecimal():
            print(f"Your time of departure is {time}, thank you for staying at the Grotto!")
            return True
        else:
            print(f"{time} is incorrect, please enter a new value!")
            return False
    def nights_to_stay(self, night_count):
        """
        A method that validates the number of nights a user will stay.
        """
        if night_count.isdigit():
            print("Great!")
            return True
        else:
            print(f"{night_count} is not in digit format, please try again!")
            return False
    def rent_room(self,rooms_available):
        """
        A method that portrays the act of renting a room.
        """
        if rooms_available.isdigit():
            print("Sorry, but there are no more room available to rent at the moment!")
            return False
        else:
            if rooms_available.isdigit() and rooms_available > 0:
                rooms_available = rooms_available - 1
                return True
            else:
                print("Sorry, but we do not have any other rooms available currently.\nTry again later.")
                return False