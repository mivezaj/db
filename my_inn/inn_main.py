#Matthew Ivezaj
#importing modules.
from classes.validation_inn import *

from classes.database_access import DB_Connect
my_inn = Inn("10", "4", "2", "200.00", "7")
#Creating login connection
my_db = DB_Connect('root', '', 'python_projects')
#Setting a flag.
not_inn = False
#Testing for the users response.
while not not_inn:
    #Giving the user a list of choices.
    print("Would you like to stay at an Inn tonight?\nMargrave: $100/night.\nHigh Oaks: $96/night.\nBelton: $167/night\n")
    #Asking the user for their input.
    user_input = input("Please enter the first letter of your choice here: ")
    #Setting a flag.
    not_stay = False
########################################### START TESTING FOR INPUT GATHERING ###########################################
    while not not_stay:
        #Handling the case where the user inputs M for Margrave.
        if user_input.upper() == "M":
            #Asking for user input.
            nights_to_stay = input("You have chosen to stay at the Margrave, your fee will be $100 per night.\nHow many nights would you like to stay?\nPlease enter a numerical value to proceed: ")
            #Finding the product of the nights they want to stay by. 
            amount = float(nights_to_stay) * 100
            #Notifying the user what the total will be.
            print(f"{amount} is your total.") 

        #Handling the case where the user inputs H for High Oaks.
        elif user_input.upper() == "H":
            #Asking the user for input.
            nights_to_stay = input("You have chosen to stay at the High Oaks, your fee will be $96 per night.\nHow many nights would you like to stay?\nPlease enter a numerical value to proceed: ")
            #Finding the product of the nights they want to stay by. 
            amount = float(nights_to_stay) * 96
            #Notifying the user what the total will be.
            print(f"{amount} is your total.") 
        #Handling the case where the user inputs B for Margrave.
        elif user_input.upper() == "B":
            #Asking the user for input.
            nights_to_stay = input("You have chosen to stay at the High Oaks, your fee will be $167 per night.\nHow many nights would you like to stay?\nPlease enter a numerical value to proceed: ")
            #Finding the product of the nights they want to stay by.
            amount = float(nights_to_stay) * 96
            #Notifying the user what the total will be.
            print(f"{amount} is your total.") 
        #Handling the case where the user has not entered in one of the valid options.
        else:
            print(f"Sorry, but {nights_to_stay} is not a valid choice, please try again!")
########################################### End TESTING FOR INPUT GATHERING ###########################################
########################################### BEGIN INSERTING DATA INTO PHPMYADMIN ###########################################
        my_db.executeQuery("INSERT INTO playlist (artist_name, album_name, song_name) VALUES (\'" + artist_name+"\',\'"+album_name +"\',\'"+ song_name +"\')")
        #Saving to the database.
        my_db.conn.commit()
########################################### END OF INSERTING DATA INTO PHPMYADMIN ###########################################